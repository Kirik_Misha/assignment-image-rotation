#include "../include/file.h"

enum open_file_status check_open_file(FILE **file, const char *name, const char *mode)
{
    *file = fopen(name, mode);
    return !file ? NO_FILE : OPEN_OK;
}
