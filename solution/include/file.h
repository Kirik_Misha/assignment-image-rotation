#ifndef FILE_H
#define FILE_H

#include <stdio.h>
 
enum open_file_status{
    OPEN_OK=0,
    EMPTY_FILE,
    NO_FILE,
};

enum open_file_status check_open_file(FILE **file, const char *name, const char *mode);

#endif
