#ifndef BMP_H
#define BMP_H
#include "image.h"
#include <stdint.h>
#include <stdio.h>

enum upload_status
{
    UPLOAD_OK = 0,
    UPLOAD_INVALID_HEADER,
    ERROR_UPLOAD,
};

enum unload_status
{
    UNLOAD_OK = 0,
    INVALID_SOURCE,
    UNLOAD_INVALID_HEADER,
    ERROR_UNLOAD,
};

enum upload_status check_bmp_upload(FILE *in, struct image *img);
enum unload_status check_bmp_unload(FILE *out, struct image *img);

#endif
