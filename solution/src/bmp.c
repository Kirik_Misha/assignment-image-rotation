#include "../include/bmp.h"
#include "../include/image.h"

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static const uint32_t TYPE = 19778;
static const uint32_t RESERVED = 0;
static const uint32_t SIZE = 40;
static const uint16_t PLANES = 1;
static const uint16_t BIT_COUNT = 24;
static const uint32_t COMPRESSION = 0;
static const uint32_t PIXELS_PER_METER = 2834;
static const uint32_t CLR_USED = 0;
static const uint32_t CLR_IMPORTANT = 0;

static struct bmp_header create_bmp_header(uint64_t width, uint64_t height, uint64_t image_size)
{
    return (struct bmp_header){
        .bfType = TYPE,
        .bfSize = image_size + sizeof(struct bmp_header),
        .bfReserved = RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION,
        .biSizeImage = image_size,
        .biXPelsPerMeter = PIXELS_PER_METER,
        .biYPelsPerMeter = PIXELS_PER_METER,
        .biClrUsed = CLR_USED,
        .biClrImportant = CLR_IMPORTANT,
    };
}

enum upload_status check_bmp_upload(FILE *in, struct image *img)
{
    if (!in)
        return ERROR_UPLOAD;
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return UPLOAD_INVALID_HEADER;

    *img = image_create(header.biHeight, header.biWidth);
    if (fseek(in, header.bOffBits, SEEK_SET) != 0)
    {
        image_destroy(img);
        return ERROR_UPLOAD;
    }

    const size_t size_per_row = (size_t)(img->width) * sizeof(struct pixel);
    const long padding = (long)img->width % 4;

    for (uint64_t i = 0; i < img->height; i++)
    {
        if (fread(img->data + i * (img->width), size_per_row, 1, in) != 1 ||
            fseek(in, padding, SEEK_CUR))
        {
            image_destroy(img);
            return ERROR_UPLOAD;
        }
    }

    return UPLOAD_OK;
}

enum unload_status check_bmp_unload(FILE *out, struct image *img)
{

    if (img->width == 0 || img->height == 0 || img->data == NULL)
    {
        return INVALID_SOURCE;
    }

    const long padding = (long)img->width % 4;
    const size_t size_per_row = img->width * sizeof(struct pixel);

    const struct bmp_header header = create_bmp_header(img->width, img->height, (size_per_row + padding) * img->height);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1 ||
        fseek(out, header.bOffBits, SEEK_SET))
    {
        return ERROR_UNLOAD;
    }

    const uint8_t padding_bytes[4] = {0};
    
    for (uint64_t i = 0; i < img->height; i++)
    {
        if (fwrite(img->data + i * img->width, size_per_row, 1, out) != 1 ||
            fwrite(padding_bytes, padding, 1, out) != 1)
        {
            return ERROR_UNLOAD;
        }
    }

    return UNLOAD_OK;
}
