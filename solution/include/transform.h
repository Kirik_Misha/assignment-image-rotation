#ifndef TRANSFORM_H
#define TRANSFORM_H
#include "file.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


struct image rotate_image(struct image source);

#endif
