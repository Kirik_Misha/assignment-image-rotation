#include "bmp.h"
#include "../include/image.h"
#include "transform.h"
#include <stdio.h>

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        fprintf(stderr,
                "Enter <input_file_name> and "
                "<output_file_name> arguments\n");
        return 1;
    }
    const char *in = argv[1];
    const char *out = argv[2];

    FILE *fin = NULL;
    enum open_file_status result_open = check_open_file(&fin, in, "rb");
    if (!fin)
    {
        fprintf(stderr, "Input file pointer broke\n");
        return 1;
    }
    switch (result_open)
    {
    case NO_FILE:
        fprintf(stderr, "No such file or directory: %s\n", in);
        return EMPTY_FILE;
    case EMPTY_FILE:
        return EMPTY_FILE;
    case OPEN_OK:
        fprintf(stdin, "Input file opened\n");
        break;
    }

    FILE *fout = NULL;
    int result_close = check_open_file(&fout, out, "wb");
    switch (result_close)
    {
    case NO_FILE:
        fprintf(stderr, "No such file or directory: %s\n",
                out);
        fclose(fout);
        fclose(fin);
        return EMPTY_FILE;
    case EMPTY_FILE:
        fprintf(stderr, "Output file pointer broke\n");
        fclose(fout);
        fclose(fin);
        return EMPTY_FILE;
    case OPEN_OK:
        fprintf(stdin, "Output file opened\n");
        break;
    }

    struct image image = {0};

    enum upload_status result_read = check_bmp_upload(fin, &image);
    switch (result_read)
    {
    case UPLOAD_OK:
        fprintf(stdin, "Image loaded successfully\n");
        break;
    case UPLOAD_INVALID_HEADER:
        fprintf(stderr, "File has an invalid header\n");
        return 1;
    case ERROR_UPLOAD:
        fprintf(stderr, "Read error appeared\n");
        return 1;
    }

    struct image new_image = rotate_image(image);
    enum unload_status result_write = check_bmp_unload(fout, &new_image);
    switch (result_write)
    {
    case UNLOAD_OK:
        fprintf(stdin, "Image saved successfully\n");
        break;
    case INVALID_SOURCE:
        fprintf(stderr, "Image broke 2\n");
        return 1;
    case UNLOAD_INVALID_HEADER:
        fprintf(stderr, "Error appeared while writing new image's header\n");
        return 1;
    case ERROR_UNLOAD:
        fprintf(stderr, "Error appeared while writing new image\n");
        return 1;
    }
    image_destroy(&new_image);

    image_destroy(&image);
    fclose(fin);
    fclose(fout);

    return 0;
}
