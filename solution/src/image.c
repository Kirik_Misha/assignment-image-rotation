#include "../include/image.h"
#include "../include/bmp.h"

struct image image_create(uint64_t height, uint64_t width) {
    struct pixel* data = malloc(height * width * sizeof(struct pixel));
    struct image result;
    result.data = data;
    result.height = height;
    result.width = width;
    return result;
}

struct pixel image_get_pixel(const struct image* image, uint64_t x, uint64_t y) {
    return image->data[image->width * y + x];
}

void image_set_pixel(struct image* image, struct pixel pixel, uint64_t x, uint64_t y) {
    image->data[image->width * y + x] = pixel;
}

void image_destroy(struct image const * image) { 
    free(image->data);
}
