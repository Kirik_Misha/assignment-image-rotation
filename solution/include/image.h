#ifndef IMAGE_H
#define IMAGE_H
#include "file.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) pixel
{
    uint8_t blue, green, red;
};

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

enum rotate_status
{
    OK = 0,
    ERROR_ROTATE_STATUS,
};

struct image image_create(uint64_t height, uint64_t width);

struct pixel image_get_pixel(const struct image *image, uint64_t x, uint64_t y);

void image_set_pixel(struct image *image, struct pixel pixel, uint64_t x, uint64_t y);

void image_destroy(struct image const *image);

#endif
