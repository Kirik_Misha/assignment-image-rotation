#include "image.h"

struct image rotate_image(struct image source) {
    struct image result = image_create(source.width, source.height);
    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            image_set_pixel(&result, image_get_pixel(&source, x, y), source.height - 1 - y, x);
        }
    }
    return result;
}
